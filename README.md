# Structuring Machine Learning Workflow on AWS

Using AWS services, this project aims at creating a typical workflow for a machine learning project, starting from data preprocessing to model training, deployment and then monitoring using the orchestration of AWS Step Functions and Lambda function.

## Project Requirment

1. Have an account on AWS.
2. Launch a notebook on Sagemaker
3. Create an S3 bucket (you can use the defualt one created when launching your first SageMaker app)
4. Assign the `Python 3 (Data Science)` kernel and the `ml.t3.medium` instance.
5. Attach SagemakerFullAccess policy to S3, Lambda and Step Funtions using the IAM role.
6. Attach StepFunctionFullAcess policy to Lambda.

## Project End Goal

The project goal is to set a workflow for a machine learning project. This workflow will organize the daily tasks a ML engineer will do in order to maintain the model being working smoothly and reporting all the required metrics chosen by the project owner. The main idea is to learn the concept of _orchestration_; organizing and packging the workflow pahses in specific order that leads to automation.
